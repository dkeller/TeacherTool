<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 20/11/2017
 * Time: 14:28
 */
include_once('Connexion/dbcon.php');

function insertExo($name, $FK_idCourse_def, $description)
{
    $connexion = ConnexionDB();

    if (null === getExoByName($name)) {

        $sql = "insert into mydb.Exo(name, FK_idCourse_def, description)
                        values('$name', '$FK_idCourse_def', '$description')";


        mysqli_query($connexion, $sql); // INSERT INTO DATABASE
        return mysqli_insert_id($connexion) ; // RETURNS LAST ID

    }
    else return null;
}


function getExoByName($name){
    $connexion = ConnexionDB();

    $sql = "SELECT * FROM mydb.Exo WHERE name='$name'";
    $result = mysqli_query($connexion, $sql);

    $row = mysqli_fetch_assoc($result);

    return $row;
}

function getAllExo(){
    $connexion = ConnexionDB();
    $array = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Exo ";
    $result = mysqli_query($connexion, $sql);

    while ($row = mysqli_fetch_assoc($result)){
        $array[$i] = $row;
        $i++;
    };

    // delete result set
    mysqli_free_result($result);

    return $array;
}



//var_dump(insertExo("Couleur de fond", 1,"test" ));
// var_dump(getExoByName("test"));
// var_dump(getAllExo());
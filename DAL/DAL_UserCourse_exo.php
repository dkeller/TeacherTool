<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 20/11/2017
 * Time: 14:28
 */
include_once('Connexion/dbcon.php');
date_default_timezone_set('America/Montreal');

function insertUserCourse_Exo($FK_idCourse_exo, $FK_idUser, $urlFile, $dateDeposit, $nameFile, $quality)
{
    $connexion = ConnexionDB();

    if (null === getUserCourse_exoByIdCourse_exoAndByIdUser($FK_idCourse_exo, $FK_idUser)) {
    $sql = "insert into mydb.UserCourse_exo(FK_idCourse_exo, FK_idUser, urlFile, dateDeposit, nameFile, quality)
                        values('$FK_idCourse_exo', '$FK_idUser', '$urlFile', '$dateDeposit', '$nameFile', '$quality')";

    mysqli_query($connexion, $sql); // INSERT INTO DATABASE
    return mysqli_insert_id($connexion) ; // RETURNS LAST ID
    }
    else return null;
}


/**
 * @param $idCourse_exo
 * @param $idUser
 * @return array if exists -> WARNING MESSAGE to be shown to user, null if it doesn't exist
 */
function getUserCourse_exoByIdCourse_exoAndByIdUser($idCourse_exo, $idUser){
    $connexion = ConnexionDB();

    $sql = "SELECT * FROM mydb.UserCourse_exo WHERE FK_idCourse_exo='$idCourse_exo' AND FK_idUser = '$idUser'";
    $result = mysqli_query($connexion, $sql);

    $row = mysqli_fetch_assoc($result);

    // delete result set
    //mysqli_free_result($result);

    return $row;
}


function getAllUserCourse_exo(){
    $connexion = ConnexionDB();
    $array = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.UserCourse_exo ";
    $result = mysqli_query($connexion, $sql);

    while ($row = mysqli_fetch_assoc($result)){
        $array[$i] = $row;
        $i++;
    };

    // delete result set
    mysqli_free_result($result);

    return $array;
}

$date = date('Y-m-d H:i:s');
$datePlus = strtotime($date.'+ 1 days');

var_dump(insertUserCourse_Exo(1, 2,"/test.php", $date, "depôt devoir 1", 8 ));
var_dump(getAllUserCourse_exo());
<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 14/11/2017
 * Time: 09:23
 */
require_once('DAL/DAL_User.php');

const P_USERNAME = 'p_username';
const P_PASSWORD = 'p_password';
const P_CONNX = 'p_connx';
const P_DECONNX = 'p_deconnx';

if (PHP_SESSION_NONE === session_status())
    session_start();

// BUTTON Connexion
if (array_key_exists(P_USERNAME, $_POST)){
    if (authenticate($_POST[P_USERNAME], $_POST[P_PASSWORD]) ) {
        $_SESSION[P_USERNAME] = $_POST[P_USERNAME];

        // redirect to app
        header("Location: app.php ");
    }

    function registerUser($username, $password, $fname, $lname, $type, $courriel)
    {
        //clean user input to prevent sql injection
        $error = false;

        $username = protectionSQLInjection($username);
        $password = protectionSQLInjection($password);
        $fname = protectionSQLInjection($fname);
        $lname = protectionSQLInjection($lname);
        $type = protectionSQLInjection($type);
        $courriel = protectionSQLInjection($courriel);

        //validate
        if (empty($username)) {
            $error = true;
            $errorUsername = 'Please input username';
        }

        if (empty($password)) {
            $error = true;
            $errorPassword = 'Please password';
        }

        if (empty($fname)) {
            $error = true;
            $errorFname = 'Please fname';
        }

        function user_is_logged()
        {
            return array_key_exists(P_USERNAME, $_SESSION);
        }

        function validate($input)
        {
            if (empty($input)) {
                this . $error = true;
            }
        }
    }
}

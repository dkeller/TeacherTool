<div class="container">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <img src="assets/img/isi.png" alt="image-isi">
                <h3 class="panel-title"><strong>Nouveau compte</strong></h3>
            </div>
            <form role="form">
                <div class="form-group">
                    <label for="username">Courriel</label>
                    <input type="text" class="form-control" id="username" placeholder="Votre nom d'utilisateur">
                </div>
                <div class="form-group">
                    <label for="email">Courriel</label>
                    <input type="email" class="form-control" id="email" placeholder="Votre courriel">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="votre mot de passe">
                </div>
                <div class="form-group">
                    <label for="repassword">Password</label>
                    <input type="password" class="form-control" id="repassword" placeholder="votre mot de passe encore">
                </div>
                <div class="form-group">
                    <label for="role">Rôle</label>
                    <br>
                        <input type="radio" name="role" value="teacher"> Enseignant<br>
                        <input type="radio" name="role" value="student"> Étudiant<br>
                </div>
                <span>Revenir à la page<a href="index.php"> connexion</a>
                </span>
                <br>
                <input id="btn-submit" type="submit" class="btn btn btn-success" value="Connexion">
            </form>
        </div>
    </div>
</div>
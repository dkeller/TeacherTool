<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 16/11/2017
 * Time: 10:36
 */
?>
<div id="2_deposit_exercice">
    <h1>Dépôt Devoir</h1>
    <div id="mainDiv">
        <label for="course">Cours</label>
        <select id="course" name="course">
            <option>TEST1</option>
            <option>TEST2</option>
        </select>

        <label>Exo</label>
        <select id="exo" name="exo">
            <option>Exo1</option>
            <option>Exo2</option>
        </select>

        <p class="error">! Exercice déjà déposé, la version que vous déposerez ci-dessous remplacera l'ancienne.</p>
        <p class="error">! La data limite a été atteinte.</p>
    </div>

    <p>Votre dossier à été déposé avec Succès.</p>

    <div id="dropZone">
        <span>Déposez ici</span>
    </div>
</div>

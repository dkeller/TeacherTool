<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 16/11/2017
 * Time: 10:36
 */
?>

<div id="1_subscribe_student">
    <h1>Inscription Cours</h1>
    <div id="mainDiv">
        <form method="post">
            <label for="course">Cours</label>
            <select id="course" name="course">
                <option>TEST1</option>
                <option>TEST2</option>
            </select>
            <label></label>
            <button class="btn btn-success"
            >Inscription</button>

        </form>
        <span>* Vous êtes bien inscrit au cours.</span>
    </div>
</div>


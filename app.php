<?php
require_once('BLL/BLL_User.php');
require_once ('BLL/redirection.php');

?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <?php require_once('views/toggleMenuTemplate/headToggleMenu.php') ?>

    <title>Accueil</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

</head>
    <body>
    <?php   if ($usertype === "student") {
        // SIDEBAR
        require_once('views/toggleMenuTemplate/BEGINwrapperToggleMenuStudent.php');

        // REDIRECTION
        switch ($page){
            case "1" :
                require_once('views/1_subscribe_student.php');
                break;
            case "2" :
                require_once('views/2_deposit_exercice.php');
                break;
            case "3":
                require_once('views/3_summary.php');
                break;
        }
    }
    else{
// SIDEBAR
        require_once('views/toggleMenuTemplate/BEGINwrapperToggleMenuTeacher.php');

        // REDIRECTION
        switch ($page){
            case "1" :
                require_once('views/1_subscribe_student.php');
                break;
            case "2" :
                require_once('views/2_deposit_exercice.php');
                break;
            case "3":
                require_once('views/3_summary.php');
                break;
        }
    }
?>

    <?php require_once('views/toggleMenuTemplate/ENDwrapperToggleMenu.php') ?>
    </body>
</html>
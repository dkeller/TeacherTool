<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 20/11/2017
 * Time: 14:28
 */
include_once('Connexion/dbcon.php');
date_default_timezone_set('America/Montreal');

function insertCourse_Exo($FK_idExo, $FK_idCourse, $dateOfDelivery, $description, $name)
{
    $connexion = ConnexionDB();


        $sql = "insert into mydb.Course_exo(FK_idExo, FK_idCourse, dateOfDelivery, description, name)
                        values('$FK_idExo', '$FK_idCourse', '$dateOfDelivery', '$description', '$name')";


        mysqli_query($connexion, $sql); // INSERT INTO DATABASE
        return mysqli_insert_id($connexion) ; // RETURNS LAST ID

}


function getListCourse_exoByIdExo($idExo){
    $connexion = ConnexionDB();
    $listCourse = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Course_exo WHERE FK_idExo='$idExo'";
    $result = mysqli_query($connexion, $sql);

    while($row = mysqli_fetch_assoc($result)){
        $listCourse[$i] = $row;
        $i++;
    };

    // delete result set
    //mysqli_free_result($result);

    return $listCourse;
}

function getListCourse_exoByIdCourse($idCourse){
    $connexion = ConnexionDB();
    $listCourse = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Course_exo WHERE FK_idCourse='$idCourse'";
    $result = mysqli_query($connexion, $sql);

    while($row = mysqli_fetch_assoc($result)){
        $listCourse[$i] = $row;
        $i++;
    };

    return $listCourse;
}

function getAllCourse_Exo(){
    $connexion = ConnexionDB();
    $array = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Course_exo ";
    $result = mysqli_query($connexion, $sql);

    while ($row = mysqli_fetch_assoc($result)){
        $array[$i] = $row;
        $i++;
    };

    // delete result set
    mysqli_free_result($result);

    return $array;
}

$date = date('Y-m-d H:i:s');
$datePlus = strtotime($date.'+ 1 days');

// var_dump(insertCourse_Exo(1, 1,$date, "devoir", "devoir fond" ));
// var_dump(getExoByName("test"));
// var_dump(getAllCourse_Exo());
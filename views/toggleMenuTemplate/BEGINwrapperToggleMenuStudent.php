<div id="wrapper" class="toggled">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <h2>TeacherTool</h2>
            <li>
                <a href="sing-up-course-page.php">Inscription cours</a>
            </li>
            <li>
                <a href="depot-homework-page.php">Dépôt devoir</a>
            </li>
            <li>
                <a href="course-summary-page.php">Sommaire</a>
            </li>
            <li>
                <a href="account-page.php">Mon Compte</a>
            </li>
        </ul>
        <a id="form-logout" class="btn btn-danger" href="index.php">Déconnexion</a>
    </div>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Menu</a>
        </div>


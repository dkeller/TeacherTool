<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 16/11/2017
 * Time: 10:36
 */
?>
<div id="depot-homework">
    <h1>Dépôt Devoir</h1>
    <div id="mainDiv">
        <label for="course">Cours</label>
            <select id="course" name="course">
                <option>cours1</option>
                <option>cours2</option>
            </select>

        <label for="exercise">Exercice</label>
            <select id="exercise" name="exercise">
                <option>Exo1</option>
                <option>Exo2</option>
            </select>
        <p class="error">! Exercice déjà déposé, la version que vous déposerez ci-dessous remplacera l'ancienne.</p>
        <p class="error">! La data limite a été atteinte.</p>
    </div>

    <span>Votre dossier à été déposé avec Succès.</span>

    <div id="drop-zone">
        <p>Déposez ici</p>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 16/11/2017
 * Time: 10:36
 */
?>

<div id="sing-up-course">
    <h1>Inscription Cours</h1>
    <div id="mainDiv">
        <form method="post">
            <label for="course">Cours</label>
            <select id="course" name="course">
                <option>TEST1</option>
                <option>TEST2</option>
            </select>
            <label></label>
            <button class="btn btn-success"
            >Inscription</button>
        </form>
    </div>
    <p>* Vous êtes bien inscrit au cours.</p>

    <h3>Voici la liste de vos cours :</h3>
    <ul>
        <li>Liste des cours inscrits</li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>


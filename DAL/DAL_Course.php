<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 20/11/2017
 * Time: 14:28
 */
include_once('Connexion/dbcon.php');
date_default_timezone_set('America/Montreal');

function insertCourse($startDate, $endDate, $FK_idCourse_def, $FK_idUser, $description, $name){
    $connexion = ConnexionDB();


        $sql = "insert into mydb.Course(startDate, endDate, FK_idCourse_def, FK_idUser, description, name)
                        values( '$startDate', '$endDate', '$FK_idCourse_def', '$FK_idUser', '$description', '$name')";

        mysqli_query($connexion, $sql); // INSERT INTO DATABASE
        return mysqli_insert_id($connexion) ; // RETURNS LAST ID
}

function getAllCourse(){
    $connexion = ConnexionDB();
    $array = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Course ";
    $result = mysqli_query($connexion, $sql);

    while ($row = mysqli_fetch_assoc($result)){
        $array[$i] = $row;
        $i++;
    };

    // delete result set
    mysqli_free_result($result);

    return $array;
}

function getCourseByIdUser($idUser){
    $connexion = ConnexionDB();
    $array = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Course WHERE FK_idUser='$idUser'  ";
    $result = mysqli_query($connexion, $sql);

    while ($row = mysqli_fetch_assoc($result)){
        $array[$i] = $row;
        $i++;
    };

    // delete result set
    mysqli_free_result($result);

    return $array;
}

$date = date('Y-m-d H:i:s');
$datePlus = strtotime($date.'+ 1 days');


// var_dump( date('Y-m-d H:i:s', $datePlus) );

//var_dump(insertCourse(  date('Y-m-d H:i:s') , date('Y-m-d H:i:s', $datePlus)  , 1, 1, "test", "cours septembre"));
//var_dump(getAllCourse());
var_dump(getCourseByIdUser(1));

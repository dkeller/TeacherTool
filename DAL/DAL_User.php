<?php
    include_once('Connexion/dbcon.php');

    function getUserByUsername($username){
        $connexion = ConnexionDB();

        $sql = "SELECT * FROM user WHERE username='$username'";
        $result = mysqli_query($connexion, $sql);

       $row = mysqli_fetch_assoc($result);

        return $row;
    }

/**
 * @param $username
 * @param $password
 * @param $fname
 * @param $lname
 * @param $type
 * @param $courriel
 * @return null if username already exists in db, inserts user if username doesn't exist in database and returns id
 */
    function insertUser($username, $password, $fname, $lname, $type, $courriel)
    {
        $connexion = ConnexionDB();

        if (null === getUserByUsername($username)) {
            $password = password_hash($password, PASSWORD_DEFAULT);

            $sql = "insert into mydb.user(username, password, fname, lname, type, courriel)
                        values('$username', '$password', '$fname', '$lname', '$type', '$courriel')";

            mysqli_query($connexion, $sql); // INSERT INTO DATABASE
            return mysqli_insert_id($connexion) ; // RETURNS LAST ID
        }
        else return null;
    }

    function authenticate($username, $password) {
    $user = getUserByUsername($username);

    return (password_verify($password,$user['password']) ) ;
    }

    // var_dump(getUserByUsername("brice"));
    //var_dump(insertUser('brice12','456', 'bean', 'mister', 'student', 'test@gmail.com'));
   // var_dump(authenticate('brice1', '456'));
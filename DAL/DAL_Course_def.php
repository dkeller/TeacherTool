<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 20/11/2017
 * Time: 14:28
 */
include_once('Connexion/dbcon.php');

function insertCourse_def($name, $description){
    $connexion = ConnexionDB();

    if (null === getCourse_def($name)) {
        $sql = "insert into mydb.Course_def(name, description)
                        values('$name', '$description')";

        mysqli_query($connexion, $sql); // INSERT INTO DATABASE
        return mysqli_insert_id($connexion) ; // RETURNS LAST ID
    }
    else return null;
}

function getAllCourse_def(){
    $connexion = ConnexionDB();
    $array = [];
    $i = 0;

    $sql = "SELECT * FROM mydb.Course_def ";
    $result = mysqli_query($connexion, $sql);

    while ($row = mysqli_fetch_assoc($result)){
        $array[$i] = $row;
        $i++;
    };

    // delete result set
    mysqli_free_result($result);

    return $array;
}

function getCourse_def($name){
    $connexion = ConnexionDB();

    $sql = "SELECT * FROM mydb.Course_def WHERE name='$name'  ";
    $result = mysqli_query($connexion, $sql);

    $row = mysqli_fetch_assoc($result);

    // delete result set
    mysqli_free_result($result);

    return $row;
}

    var_dump(insertCourse_def("P85", "PHP"));
    var_dump(getAllCourse_def());

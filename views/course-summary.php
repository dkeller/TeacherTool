<?php
/**
 * Created by PhpStorm.
 * User: dimitrikeller
 * Date: 16/11/2017
 * Time: 10:36
 */
?>

<div id="course-summary">
    <h1>Sommaire</h1>
    <div id="mainDiv">
        <form method="post">
            <label for="course">Cours</label>
            <select id="course" name="course">
                <option>TEST1</option>
                <option>TEST2</option>
            </select>
        </form>
    </div>

    <div id="secondDiv">
        <table>
            <tr>
                <th></th>
                <th>Nom exercice</th>
                <th>Date limite</th>
                <th>Date déposée</th>
                <th>Statut</th>
                <th>Evaluation</th>
            </tr>
            <tr>
                <td></td>
                <td>Changer couleur de fond</td>
                <td>12/12/2017</td>
                <td>11/12/2017</td>
                <td><i class="fa fa-check-circle fa-2x" aria-hidden="true"></i></td>
                <td>7/10</td>
            </tr>
            <tr>
                <td></td>
                <td>Eventlisteners</td>
                <td>13/12/2017</td>
                <td>11/12/2017</td>
                <td><i class="fa fa-times-circle fa-2x" aria-hidden="true"></i></td>
                <td>8/10</td>
            </tr>
            <tr>
                <td>TOTAL</td>
                <td></td>
                <td></td>
                <td></td>
                <td>8/10</td>
                <td>100/100</td>
            </tr>
        </table>
    </div>
</div>



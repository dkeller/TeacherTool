<?php
require_once('BLL/BLL_User.php');

        $password = trim($_POST['password']);
        $password = htmlspecialchars(strip_tags($password));

        if (loginUser($username, $password)){
            header('location: sing-up-course-page.php');
        }

?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Accueil</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>
        <div id="login" class="container">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <img src="assets/img/isi.png" alt="image-isi">
                        <h3 class="panel-title"><strong>Connexion</strong></h3>
                    </div>
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                        <div class="form-group">
                            <label for="text">Courriel</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Votre courriel">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="votre mot de passe">
                        </div>
                        <span>Créez-vous un <a href="sing-up-page.php">nouveau compte</a></span>
                        <br>
                        <input id="btn-submit" type="submit" name="btn-login" class="btn btn btn-success" value="Connexion">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>